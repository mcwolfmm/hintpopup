/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package mcwolf.HintPopup;

/**
 *
 * @author office
 */
public class HintEvent {
	private final HintPopup source;
	private final Object hint;

	public HintEvent(HintPopup source, Object hint) {
		this.source = source;
		this.hint = hint;
	}

	public Object getHint() {
		return hint;
	}

	public HintPopup getSource() {
		return source;
	}
}
