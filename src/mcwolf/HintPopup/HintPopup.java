/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mcwolf.HintPopup;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JWindow;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author office
 */
public abstract class HintPopup implements Runnable, DocumentListener, WindowListener {
	private static final int HEIGHT = 200;
	
	private final JTextField textField;
	
	private final JList<Object> list;
	private final JScrollPane scrollPane;
	private JWindow popupWindow;
	
	private List<HintListener> listeners;
	
	private volatile boolean hint;
	private volatile boolean work;
	
	private final Queue<String> task;
	
	public HintPopup(JTextField textField) {
		hint = true;
		
		this.textField = textField;
		this.textField.getDocument().addDocumentListener(this);
		
		this.textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				switch(e.getKeyCode()) {
					case KeyEvent.VK_SHIFT:
					case KeyEvent.VK_ALT:
					case KeyEvent.VK_CONTROL:
					case KeyEvent.VK_CAPS_LOCK:
						break;
					
					case KeyEvent.VK_DOWN:
						addTask(HintPopup.this.textField.getText());
						hint = false;
						
						showPopup();
						listRequestFocus();
						break;
					
					case KeyEvent.VK_ESCAPE:
					case KeyEvent.VK_ENTER:
						hidePopup();
						break;
						
					default: showPopup();
				}
			}
		});
		
		this.textField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(popupWindow != null && popupWindow.isVisible()) {
					popupWindow.setFocusableWindowState(false);
					popupWindow.toFront();
				}
			}
			
			@Override
			public void focusLost(FocusEvent e) {
				if(e.getOppositeComponent() != list) {
					hidePopup();
				}
			}
		});
		
		list = new JList<>(new HintPopupModel());
		
		list.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				switch(e.getKeyCode()) {
					case KeyEvent.VK_ENTER:
						fireHintEvent(new HintEvent(HintPopup.this, list.getSelectedValue()));
						
						textFieldRequestFocus();
						hidePopup();
						break;
						
					case KeyEvent.VK_ESCAPE:
						textFieldRequestFocus();
						hidePopup();
						break;
				}
			}
		});
		
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getClickCount() == 2) {
					fireHintEvent(new HintEvent(HintPopup.this, list.getSelectedValue()));
						
					textFieldRequestFocus();
					hidePopup();
				}
				else {
					listRequestFocus();
				}
			}
		});
		
		list.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				fireHintEvent(new HintEvent(HintPopup.this, list.getSelectedValue()));
			}
		});
		
		scrollPane = new JScrollPane(list);
		scrollPane.setBackground(Color.WHITE);
		scrollPane.getViewport().setBackground(Color.WHITE);
		scrollPane.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		scrollPane.setViewportBorder(BorderFactory.createEmptyBorder());

		task = new LinkedList<>();
	}
	
	private void movePopup() {
		if(popupWindow != null && popupWindow.isVisible()) {
			Point point = textField.getLocationOnScreen();
			popupWindow.setBounds(point.x, point.y + textField.getHeight(), textField.getWidth(), HEIGHT);
		}
	}
	
	private void showPopup() {
		if(popupWindow == null || !popupWindow.isVisible()) {
			Point point = textField.getLocationOnScreen();
			
			popupWindow = new JWindow(SwingUtilities.getWindowAncestor(textField));
			popupWindow.setFocusableWindowState(false);
			popupWindow.add(scrollPane);
			popupWindow.addWindowListener(this);
			popupWindow.setBounds(point.x, point.y + textField.getHeight(), textField.getWidth(), HEIGHT);
			popupWindow.setVisible(true);
			
			popupWindow.getParent().addComponentListener(new ComponentAdapter() {
				@Override
				public void componentMoved(ComponentEvent e) {
					movePopup();
				}
			});
		}
	}
	
	private void hidePopup() {
		hint = true;
		stop();
		
		if(popupWindow != null) {
			popupWindow.setVisible(false);
			popupWindow = null;
		}
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		addTask(textField.getText());
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		addTask(textField.getText());
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
		addTask(textField.getText());
	}

	@Override
	public void windowOpened(WindowEvent e) {
		start();
	}

	@Override
	public void windowClosing(WindowEvent e) {
		stop();
	}

	@Override
	public void windowClosed(WindowEvent e) {
		stop();
	}

	@Override
	public void windowIconified(WindowEvent e) {
		stop();
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		start();
	}

	@Override
	public void windowActivated(WindowEvent e) {
		start();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		stop();
	}
	
	private void listRequestFocus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				popupWindow.setFocusableWindowState(true);
				popupWindow.requestFocusInWindow();
				popupWindow.toFront();
				
				list.requestFocus();
				list.setSelectedIndex(0);
			}
		});
	}
	
	private void textFieldRequestFocus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {		
				textField.requestFocus();
			}
		});
	}
	
	public void addHintListener(HintListener l) {
		if(listeners == null) {
			listeners = new ArrayList<>();
		}
		
		listeners.add(l);
	}
	
	public void removeHintListener(HintListener l) {
		if(listeners == null) {
			return;
		}
		
		listeners.remove(l);
	}
	
	private void fireHintEvent(HintEvent event) {
		if(listeners == null) {
			return;
		}
		
		for(HintListener l: listeners) {
			l.actionPerformed(event);
		}
	}
	
	private synchronized void addTask(String string) {
		if(textField.isFocusOwner() && hint) {
			task.add(string);
			HintPopup.this.notify();
		}
	}
	
	private synchronized String getTask() throws InterruptedException {
		if(task.isEmpty()) {
			HintPopup.this.wait();
		}
		
		return task.poll();
	}
	
	private void start() {
		if(work) {
			return;
		}
		
		new Thread(this).start();
	}
	
	private synchronized void stop() {
		hint = true;
		work = false;
		task.clear();
		
		HintPopup.this.notifyAll();
	}

	@Override
	public void run() {
		work = true;

		while(work) {			
			try {
				String string = getTask();
				if(string == null) {
					break;
				}
				
//				if(hint) {
					list.clearSelection();
					((HintPopupModel)list.getModel()).addList(loadInBackground(string));
//				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	protected abstract ArrayList<Object> loadInBackground(String query) throws Exception;
}
