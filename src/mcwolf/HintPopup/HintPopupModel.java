/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mcwolf.HintPopup;

import java.util.ArrayList;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 *
 * @author office
 */
public class HintPopupModel implements ListModel<Object> {

	private ArrayList<Object> objects;
	private ListDataListener l;
	
	public HintPopupModel() {
	}
	
	public void addList(ArrayList<Object> objects) {
		this.objects = objects;
		
		if(l != null) {
			l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, getSize()));
		}
	}

	@Override
	public int getSize() {
		if(objects != null) {
			return objects.size();
		}
		
		return 0;
	}

	@Override
	public Object getElementAt(int index) {
		if(index < getSize()) {
			return objects.get(index);
		}
		
		return null;
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		this.l = l;
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		if(this.l == l) {
			this.l = null;
		}
	}
}
